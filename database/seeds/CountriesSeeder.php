<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $faker;

    public function run()
    {
        $this->faker = $faker = Faker\Factory::create();
        $tests = array(
            [
                'name' => 'Perú',
                'activo' => 1
            ],
            [
                'name' => 'Venezuela',
                'activo' => 0
            ],
            [
                'name' => 'Colombia',
                'activo' => 0
            ],
            [
                'name' => 'Ecuador',
                'activo' => 0
            ],
            [
                'name' => 'Bolivia',
                'activo' => 0
            ],
            [
                'name' => 'Chile',
                'activo' => 0
            ],
            [
                'name' => 'Argentina',
                'activo' => 0
            ],
            [
                'name' => 'Uruguay',
                'activo' => 0
            ],
            [
                'name' => 'Paraguay',
                'activo' => 0
            ],
            [
                'name' => 'Panamá',
                'activo' => 0
            ],
        );

        foreach ($tests as $key) {
            DB::table('countrys')->insert($key);
        }

    }
}
