<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $faker;

    public function run()
    {
        $this->faker = $faker = Faker\Factory::create();
        $tests = array(
            [
                'name' => 'Administrador',
                'email' => 'admin@admin.com',
                'level' => 'admin',
                'password' => bcrypt('12345678')
            ]
            ,
            [
                'name' => 'Liquidador 1',
                'email' => 'liquidador1',
                'level' => 'liquidador',
                'password' => bcrypt('123')
            ],
            [
                'name' => 'Liquidador 2',
                'email' => 'liquidador2',
                'level' => 'liquidador',
                'password' => bcrypt('123')
            ],
            [
                'name' => 'Agente 1',
                'email' => 'agente1',
                'level' => 'agent',
                'password' => bcrypt('123')
            ],
            [
                'name' => 'Agente 2',
                'email' => 'agente2',
                'level' => 'agent',
                'password' => bcrypt('123')
            ],
            [
                'name' => 'Agente 3',
                'email' => 'agente3',
                'level' => 'agent',
                'password' => bcrypt('123')
            ],
            [
                'name' => 'Agente 4',
                'email' => 'agente4',
                'level' => 'agent',
                'password' => bcrypt('123')
            ]
            
        );

        foreach ($tests as $key) {
            DB::table('users')->insert($key);
        }

    }
}
