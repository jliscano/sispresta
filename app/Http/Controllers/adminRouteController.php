<?php

namespace App\Http\Controllers;

use App\db_countries;
use App\db_wallet;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class adminRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $level= Auth::user()->level;
        $id_liquidador= Auth::user()->id;
        $nombre=Auth::user()->name;

        $liquidadores=array();
        if($level=='admin'){
            //si es admin entonces lista los liquidadores
            $data = db_wallet::rightjoin('users','users.id','=','wallet.id_supervisor');
        }
        //$data = db_countries::all();
        
        return view('admin.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $level= Auth::user()->level;
        $id_liquidador= Auth::user()->id;
        $nombre=Auth::user()->name;
        $liquidadores=array();
        if($level=='admin'){
            //si es admin entonces lista los liquidadores
            $liquidadores = User::all()->where('level','=','liquidador');
        }
        //$data = db_countries::all();
        $data = db_countries::all()->where('activo',1);
        
        $data = array(
            'countries' => $data,
            'id_liquidador'=> $id_liquidador,
            'level'=> $level,
            'nombre'=>$nombre,
            'liquidadores' => $liquidadores
        );
        //return view('admin.route_create',$data);
        return view('admin.route_create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->name;
        $country = $request->country;
        $address = $request->address;
        $supervisor = $request->id_liquidador;
        $values = array(
            'name' => $name,
            'created_at' => Carbon::now(),
            'country' => $country,
            'address' => $address,
            'id_supervisor' => $supervisor
        );
        db_wallet::insert($values);

        return redirect('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
