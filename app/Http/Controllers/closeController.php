<?php

namespace App\Http\Controllers;

use App\db_bills;
use App\db_close_day;
use App\db_credit;
use App\db_summary;
use App\db_supervisor_has_agent;
use App\db_wallet;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class closeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //echo "aqui";exit();
        /*
SELECT COALESCE(users.name,' ',users.last_name) as nombre, wallet.name,wallet.address, DATE_FORMAT(credit.created_at, "%d/%m/%Y") as fecha, COALESCE(sum(credit.amount_neto),0) as ventas ,COALESCE(sum(summary.amount),0) as cobros from users
LEFT JOIN credit on(credit.id_agent=users.id)
LEFT JOIN agent_has_supervisor
on(credit.id_agent=agent_has_supervisor.id_user_agent)
LEFT JOIN wallet on(agent_has_supervisor.id_wallet=wallet.id)
LEFT JOIN summary on(DATE_FORMAT(credit.created_at, "%d/%m/%Y")=DATE_FORMAT(summary.fpago, "%d/%m/%Y"))
WHERE users.level='agent'
AND
DATE_FORMAT(credit.created_at, "%d/%m/%Y") NOT IN(select DATE_FORMAT(not_pay.created_at, "%d/%m/%Y") FROM not_pay) 
GROUP BY DATE_FORMAT(credit.created_at, "%d/%m/%Y")
->leftJoin('credit','credit.id_agent','=','users.id')
    ->leftJoin('summary','summary.id_agent','=','users.id')
    ->leftJoin('bills','bills.id_agent','=','users.id')
->groupByRaw('DATE_FORMAT(credit.created_at,"%d/%m/%Y")')
    ->groupByRaw('DATE_FORMAT(summary.fpago,"%d/%m/%Y")')
    ->groupByRaw('DATE_FORMAT(bills.created_at,"%d/%m/%Y")')
    ->groupByRaw('agent_has_supervisor.id_wallet')
    ->groupByRaw('users.id')
    ->groupByRaw('credit.id_agent')
    ->groupByRaw('summary.id_agent')
    ->groupByRaw('bills.id_agent')
    ->groupByRaw('wallet.id')
    db_credit::raw('DATE_FORMAT(credit.created_at,"%d/%m/%Y") as fventa'),db_credit::raw('COALESCE(sum(amount_neto),0) as ventas' )),db_credit::raw('COALESCE(sum(amount_neto),0) as ventas' ))
        */
    

    $data=User::leftJoin('agent_has_supervisor','agent_has_supervisor.id_user_agent','=','users.id')
    ->leftJoin('wallet','wallet.id','=','agent_has_supervisor.id_wallet')
    ->where('users.level','=','supervisor')
    ->orWhere('users.level','=','agent')
    ->select('users.id','wallet.name as ruta','users.name as nombre','users.last_name as apellido','wallet.address','users.id as iduser',User::raw(" '' as fechas "))
    ->get();

    foreach ($data as $datum){
        $iduser=$datum->iduser;
        $ftope=db_close_day::where('id_agent','=',$iduser)->max('created_at');
        if(is_null($ftope) )
            $ftope='2023-01-01';
        //else
          //  $ftope=date('Y-m-d'.strtotime($ftope));
        
        $ventas_db=db_credit::where('id_agent','=',$iduser)
        ->groupByRaw('DATE_FORMAT(credit.created_at,"%d/%m/%Y")')
        ->groupByRaw('credit.id_agent')
        ->whereDate('credit.created_at','>',$ftope)
        ->select('credit.created_at')
        ->get();
        foreach($ventas_db as $venta){
            $datum->fechas.=date('Y-m-d',strtotime($venta->created_at)).'!';
        }
        $cobros_db=db_summary::where('id_agent','=',$iduser)
        ->groupByRaw('DATE_FORMAT(summary.fpago,"%d/%m/%Y")')
        ->groupByRaw('summary.fpago')
        ->whereDate('summary.fpago','>',$ftope)
        ->select('summary.fpago')
        ->get();
        foreach($cobros_db as $cobro){
            $datum->fechas.=date('Y-m-d',strtotime($cobro->fpago)).'!';
        }
        $agasto=array();
        $gastos_db=db_bills::where('id_agent','=',$iduser)
        ->groupByRaw('DATE_FORMAT(bills.created_at,"%d/%m/%Y")')
        ->groupByRaw('bills.created_at')
        ->whereDate('bills.created_at','>',$ftope)
        ->select('bills.created_at')
        ->get();
        foreach($gastos_db as $gasto){
            $datum->fechas.=date('Y-m-d',strtotime($gasto->created_at)).'!';
        }
    }
/*
        $data = db_supervisor_has_agent::where('agent_has_supervisor.id_supervisor',Auth::id())
            ->join('users','agent_has_supervisor.id_user_agent','=','users.id')
            ->get();

        foreach ($data as $datum){
            $datum->show = true;
            $datum->wallet_name = db_wallet::where('id',$datum->id_wallet)->first()->name;
            $summary=db_summary::whereDate('fpago','=',Carbon::now()->toDateString())
                ->where('id_agent',$datum->id_user_agernt)
                ->exists();

            if($summary){
                $datum->show = true;
            }

            $credit=db_credit::whereDate('created_at','=',Carbon::now()->toDateString())
                ->where('id_agent',$datum->id_user_agernt)
                ->exists();

            if($credit){
                $datum->show = true;
            }

            $close_day=db_close_day::where('id_agent',$datum->id_user_agent)
                ->whereDate('created_at','=',Carbon::now()->toDateString())
                ->exists();
            if($close_day){
                $datum->show = false;
            }

        }
*/

        $data = array(
            'clients' => $data,
            'today' => Carbon::now()->toDateString(),
        );
        return view('supervisor_agent.indexclose',$data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        /*
            $base_amount = db_supervisor_has_agent::where('id_user_agent',$id)->first()->base;
            $today_amount = db_summary::whereDate('created_at', '=', Carbon::now()->toDateString())
                ->where('id_agent',$id)
                ->sum('amount');
            $today_sell = db_credit::whereDate('created_at','=',Carbon::now()->toDateString())
                ->where('id_agent',$id)
                ->sum('amount_neto');
            $bills = db_bills::whereDate('created_at','=',Carbon::now()->toDateString())
                ->sum('amount');
            $total = floatval($base_amount+$today_amount)-floatval($today_sell+$bills);
            $average = 1000;

            $data = array(
                'base' => $base_amount,
                'today_amount' => $today_amount,
                'today_sell' => $today_sell,
                'bills' => $bills,
                'total' => $total,
                'average' => $average,
                'user' => User::find($id)
            );


        return view('supervisor_agent.close',$data);
        */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $total = $request->total_today;
        $base_total = $request->base_amount_total;
        $total_calculado = $request->base_amount_total;
        $entregado  = $request->entregado;
        $diferencia = $request->diferencia;
        if($diferencia!=0){
            $base_total=$entregado;
        }
        $fecha=date('Y-m-d',strtotime($request->fecha));

        if(!isset($total)){return 'Total vacio';};
        if(!isset($base_total)){return 'Base vacio';};

        db_supervisor_has_agent::where('id_user_agent',$id)
            ->where('id_supervisor',Auth::id())
            ->update(['base'=>$total]);

        $values = array(
            'id_agent' => $id,
            'id_supervisor' => Auth::id(),
            'created_at' => $fecha,
            'total' => $total,
            'total_calculado' => $total_calculado,
            'base_before' => $base_total,
            'entregado' => $entregado,
            'diferencia' => $diferencia
        );
        db_close_day::insert($values);

        return redirect('supervisor/close');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function close_automatic(){
        $data_agents = db_supervisor_has_agent::all();

        foreach ($data_agents as $d){

            $base_amount = db_supervisor_has_agent::where('id_user_agent',$d->id_user_agent)->first()->base;
            $today_amount = db_summary::whereDate('created_at', '=', Carbon::now()->toDateString())
                ->where('id_agent',$d->id_user_agent)
                ->sum('amount');
            $today_sell = db_credit::whereDate('created_at','=',Carbon::now()->toDateString())
                ->where('id_agent',$d->id_user_agent)
                ->sum('amount_neto');
            $bills = db_bills::whereDate('created_at','=',Carbon::now()->toDateString())
                ->sum('amount');
            $total = floatval($base_amount+$today_amount)-floatval($today_sell+$bills);


            db_supervisor_has_agent::where('id_user_agent',$d->id_user_agent)
                ->where('id_supervisor',Auth::id())
                ->update(['base'=>$total]);

            $values = array(
                'id_agent' => $d->id_user_agent,
                'id_supervisor' => Auth::id(),
                'created_at' => Carbon::now(),
                'total' => $total,
                'base_before' => $base_amount,

            );

            if(!db_close_day::whereDate('created_at','=',Carbon::now()->toDateString())->exists()){
                db_close_day::insert($values);
            }

        }
        return response()->json([
            'status' => 'success',
            'msj' => 'Cierre realizado'
        ]);
    }

    public function cerrar(Request $request){
        $cierre=$request;
        $id = $cierre->id;
        $fecha = $cierre->fechas;
        //echo $fecha;exit();
                    $base_amount = db_supervisor_has_agent::where('id_user_agent',$id)->first()->base;
            $today_amount = db_summary::whereDate('created_at', '=', $fecha)
                ->where('id_agent',$id)
                ->sum('amount');
            $today_sell = db_credit::whereDate('created_at','=',$fecha)
                ->where('id_agent',$id)
                ->sum('amount_neto');
            $bills = db_bills::whereDate('created_at','=',$fecha)
                ->sum('amount');
            $total = floatval($base_amount+$today_amount)-floatval($today_sell+$bills);
            $average = 1000;

            $data = array(
                'base' => $base_amount,
                'today_amount' => $today_amount,
                'today_sell' => $today_sell,
                'bills' => $bills,
                'total' => $total,
                'average' => $average,
                'user' => User::find($id),
                'fecha' => $fecha
            );


        return view('supervisor_agent.close',$data);
    }
}
