<?php

namespace App\Http\Controllers;

use App\db_agent_has_user;
use App\db_countries;
use App\db_credit;
use App\db_supervisor_has_agent;
use App\db_wallet;
use App\User;
use App\db_summary;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class clientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data = array(
            'wallet' => db_supervisor_has_agent::where('agent_has_supervisor.id_supervisor', Auth::id())
                ->join('wallet', 'agent_has_supervisor.id_wallet', '=', 'wallet.id')
                ->get(),
            'agents' => db_supervisor_has_agent::where('id_supervisor', Auth::id())
                ->join('users', 'id_user_agent', '=', 'users.id')->get(),
            'countries' => db_countries::all()->where('activo',1),
        );
        return view('supervisor_client.create', $data);
    }
    public function listaclientes()
    {
        $data = array(
            'clients' => db_agent_has_user::where('id_agent','=',Auth::id())
            ->where('users.level','=','user')
            ->rightjoin('users','users.id','=','agent_has_client.id_client')
            ->rightjoin('wallet','wallet.id','=','agent_has_client.id_wallet')
            ->select('users.id','users.name','users.last_name','users.nit','wallet.name as ruta')
            ->get()
        );
        return view('client.listaclientes', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_wallet = $request->wallet;
        $id_agent = $request->agent;
        $country = $request->country;

        if (!isset($id_wallet)) {
            return 'ID wallet vacio';
        };
        if (!isset($id_agent)) {
            return 'ID agente vacio';
        };
        if (!isset($country)) {
            return 'Pais vacio';
        };

        db_supervisor_has_agent::where('id_user_agent', $id_agent)->where('id_supervisor', Auth::id())
            ->update(['id_wallet' => $id_wallet]);
            
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = db_agent_has_user::where('agent_has_client.id_wallet', $id)
            ->join('users', 'agent_has_client.id_client', '=', 'users.id')
            ->join('credit', 'users.id', '=', 'credit.id_user')
            ->select(
                'users.name',
                'users.last_name',
                'users.province',
                'users.status',
                'users.id as id_user',
                DB::raw('COUNT(*) as total_credit')
            )
            ->groupBy('users.id')
            ->get();

        foreach ($data as $datum) {
            $datum->credit_inprogress = db_credit::where('status', 'inprogress')->where('id_user', $datum->id_user)->count();
            $datum->credit_close = db_credit::where('status', 'close')->where('id_user', $datum->id_user)->count();
        }
        $data = array(
            'clients' => $data
        );

        return view('supervisor_client.index', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::find($id);
        $data = array(
            'user' => $data
        );
        return view('supervisor_client.unique', $data);
    }
    public function nuevaventa($id)
    {

        
        $data = array(
            'user' => User::find($id),
            'payment_number' => DB::table('payment_number')->orderBy('name', 'asc')->get()
        );
        
        return view('client.nuevaventa', $data);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->name;
        $last_name = $request->last_name;
        $nit = $request->nit_number;
        $address = $request->address;
        $province = $request->province;
        $phone = $request->phone;
        $status = $request->status;
        

        $values = array(
            'name' => $name,
            'last_name' => $last_name,
            'nit' => $nit,
            'address' => $address,
            'province' => $province,
            'phone' => $phone,
            'status' => $status
        );

        User::where('id', $id)->update($values);
        if (db_agent_has_user::where('id_client', $id)->exists()) {
            $wallet = db_agent_has_user::where('id_client', $id)->first();
            return redirect('supervisor/client/' . $wallet->id_wallet);
        } else {
            return redirect('supervisor/client/');
        }

    }
    public function guardanuevaventa(Request $request, $id){
        if (!Auth::user()->level == 'agent') {
            return 'No tienes permisos';
        }
        $nit = $request->nit_number;
        $utility = $request->utility;
        $payment_number = $request->payment_number;
        $amount = $request->amount;
        $fdesde= $request->fdesde;
        $fhasta= $request->fhasta;
        $frecuencia=$request->frecuencia;
        $quote= ($amount + ($amount*$utility)) / $payment_number;
        
        $base = db_supervisor_has_agent::where('id_user_agent', Auth::id())->first()->base;
        $base_credit = db_credit::whereDate('created_at', Carbon::now()->toDateString())
            ->where('id_agent', Auth::id())
            ->sum('amount_neto');
        $base -= $base_credit;

        if ($amount > $base) {
            return 'No tienes dinero suficiente';
        }
        /*
        if (!User::where('nit', $nit)->exists()) {
            $id = User::insertGetId($values);
        } else {
            $id = User::where('nit', $nit)->first()->id;
        */
            if (db_agent_has_user::where('id_client', $id)->exists()) {
                $agent_data = db_agent_has_user::where('id_client', $id)->first();
                if ($agent_data->id_agent != Auth::id()) {
                    return 'Este usuario ya esta asignado a otro Agente';
                }
            }
        //}

        if (!db_agent_has_user::where('id_agent', Auth::id())->where('id_client', $id)->exists()) {
            db_agent_has_user::insert([
                'id_agent' => Auth::id(),
                'id_client' => $id,
                'id_wallet' => db_supervisor_has_agent::where('id_user_agent', Auth::id())->first()->id_wallet]);
        }

        if (db_credit::orderBy('order_list', 'DESC')->first() === null) {
            $last_order = 0;
        } else {
            $last_order = db_credit::orderBy('order_list', 'DESC')->first()->order_list;
        }
        $newDate1 = $this->acomoda_fecha($fdesde);
        $newDate2 = $this->acomoda_fecha($fhasta);

        $values = array(
            'created_at' => Carbon::now(),
            'payment_number' => $payment_number,
            'utility' => $utility,
            'frecuencia' => $frecuencia,
            'amount_neto' => $amount,
            'id_user' => $id,
            'id_agent' => Auth::id(),
            'order_list' => ($last_order) + 1,
			'fdesde'=> $newDate1,
            'fhasta'=> $newDate2
        );
        
        db_credit::insert($values);
        
        unset($values);
        $max_credito=db_credit::select(DB::raw('MAX(id) as ids'))->where('id_user',$id)->max('id');
        //echo($max_credito);exit();
        //$id_credit=$max_credito->ids;

        for($i=1;$i<=$payment_number;$i++){
        		$fecha=$this->sumarDias($newDate1, $i, $frecuencia);
        	$values = array(
        		'quote' => $quote,
		        'id_agent' => Auth::id(),
		        'id_credit'=> $max_credito,
		        'number_index' => $i,
		        'fcobro' => $fecha,
		        'created_at' => Carbon::now(),
                'condicion' => '--'
	        );
	        
	    	db_summary::insert($values);    
        	unset($values);
        }

        
        return redirect('/home');
        
    }
    public function acomoda_fecha($fecha){
    	$fecha_arreglo=explode('/',$fecha);
    	$nueva_fecha=$fecha_arreglo[2].'-'.$fecha_arreglo[1].'-'.$fecha_arreglo[0];
    	return $nueva_fecha; 
    }
    
    public function sumarDias($fecha, $dias, $frecuencia){
	if($frecuencia==0){ //diario
		$dias=$dias-1;
  	}else if($frecuencia==1){ //semanal
  		$dias=($dias-1)*7;
  	}else if($frecuencia==2){ //quincenal
  		$dias=($dias-1)*15;
  	}else if($frecuencia==3){ //mensual
  		$dias=($dias-1)*30;
  	}
	if($dias>1){
		$sumar='+'.$dias.' days';
	}else{
		$sumar='+'.$dias.' day';
	}
		
	$date_future = strtotime($sumar, strtotime($fecha));
	$date_future = date('Y-m-d', $date_future);
  		return $date_future;
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
