<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //and (Auth::user()->level != 'liquidador')
        if(isset(Auth::user()->level)){
            if ((Auth::user()->level != 'admin') ) {
                die('No tienes permisos');
            }
            return $next($request);
        }else{
            return view('welcome');
        }
        

    }
}
