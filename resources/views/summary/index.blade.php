@extends('layouts.app')
<?php
    $frecuencia=array('Diaria','Semanal','Quincenal','Mensual');
    //$calificacion=array(['1.99'=>'Manejo Pésimo'],['2.99'=>'Manejo Moroso'],['3.99'=>'Manejo Regular'],['4.89'=>'Manejo Normal'],['5'=>'Manejo Excelente']);
    
    $datetime1 = date_create($credit_data->fdesde);
    $datetime2 = date_create(date(now()));
    $interval = date_diff($datetime1, $datetime2);
    if($interval->days==0)
        $interval->days=1;
    $debe_pagar=$interval->days*$cuota;

    if($debe_pagar==0 or is_null($debe_pagar))
        $div_pagar=1;
    else
        $div_pagar=$debe_pagar;
    $calificacion='';
    if($total_pagado==0)
        $div_pagado=1;
    else
        $div_pagado=$total_pagado;

    $califica=($div_pagado*5)/($div_pagar);

    if($califica>5) $califica=5;
    if($califica>=0 && $califica<1.99){
        $calificacion='Manejo Pésimo';
    }elseif ($califica>=2 && $califica<2.99) {
        $calificacion='Manejo Moroso';
    }elseif($califica>3 && $califica<3.99){
        $calificacion='Manejo Regular';
    }elseif($califica>4 && $califica<4.89){
        $calificacion='Manejo Normal';
    }elseif ($califica>4.90 ) {
        $calificacion='Manejo Excelente';
    }

?>
@section('content')
    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="wrap">
            <section class="app-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class=" p-lg text-right">
                            <a type="button" href="{{url('client')}}" class="btn btn-inverse"><i class="fa fa-users"></i> Mostar clientes</a>
                            <a type="button" href="{{url('route')}}" class="btn btn-deepOrange"><i class="fa fa-car"></i> Continuar ruta</a>
                        </div><!-- .widget -->
                    </div>
                    <!--
                    @if(app('request')->input('show')=='last')
                        <div class="col-md-12 col-sm-12">
                            <div class="widget stats-widget">
                                <div class="widget-body clearfix">
                                    <div class="pull-left">
                                        <small class="text-color">Pago realizado</small>
                                        <h3 class="widget-title text-success">{{$last['recent']}}</h3>
                                        <small class="text-color">Saldo</small>
                                        <h3 class="widget-title text-primary">{{$last['rest']}}</h3>
                                    </div>
                                    <span class="pull-right big-icon watermark"><i class="fa fa-money"></i></span>
                                </div>
                            </div>
                        </div>
                    @endif
                    -->
    
                    <div class="col-md-12 col-sm-12">
                        <div class="panel">
                            <div class="panel-heading">
                                <h6 class="panel-title">{{'Movimientos de Pagos de Abonos del Cliente:'.$user->name}} {{$user->last_name}}</h6>
                                <h4 class="panel-title">{{'Valor total pagado por el cliente:'.$total_pagado}} </h4>
                            </div>
                            <?php
                                
                            ?>
                            <ul class="grid-container">
                                <!--<li class="grid-item">Credito: <span class="text-purple">{{$credit_data->id}}</span></li>-->
                                <li class="grid-item">Saldo Inicial: <span class="text-wwhite">{{$credit_data->amount_neto}}</span></li>
                                <li class="grid-item">Saldo Final: <span class="text-wwhite">{{$credit_data->total}}</span></li>
                                <li class="grid-item">Fecha Inicial: <span class="text-wwhite">{{ date('d/m/Y',strtotime($credit_data->fdesde)) }}</span></li>
                                <li class="grid-item">Fecha Final: <span class="text-wwhite">{{date('d/m/Y',strtotime($credit_data->fhasta))}}</span></li>
                                <li class="grid-item">Cuotas pactadas: <span class="text-wwhite">{{$credit_data->payment_number}}</span></li>
                                <li class="grid-item">Forma de Pago: <span class="text-wwhite">{{ $frecuencia[$credit_data->frecuencia]}}</span></li>
                                <li class="grid-item">Calificación: <span class="text-wwhite">{{ number_format($califica,2,'.') }} </span></li>
                                <li class="grid-item">Manejo del Crédito: <span class="text-wwhite"> {{ $calificacion }}</span></li>
                            </ul>
                        </div>
                        <ul class="">
                            <li class="">CUADRO DE CALIFICACION:</li>
                            <li class="">Desde 0 hasta 1,99: <span class=""> Manejo Pésimo</span></li>
                            <li class="">Desde 2 hasta 2,99: <span class=""> Manejo Moroso</span></li>
                            <li class="">Desde 3 hasta 3,99: <span class=""> Manejo Regular</span></li>
                            <li class="">Desde 4 hasta 4,89: <span class=""> Manejo Normal</span></li>
                            <li class="">Desde 4,90 hasta 5: <span class=""> Manejo Excelente</span></li>
                        </ul>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget p-lg">
                            <h4 class="m-b-lg">Historial</h4>
                            <table class="table agente-paymentsH-table">
                                <tbody>
                                <tr>
                                	<th>No</th>
                                    <th>Fecha Pago</th>
                                    <th>Vr.Cuota</th>
                                    <th>Cuotas Pagas</th>
                                    <th>Estado</th>
                                    <th>Vr. Pagado</th>
                                    <th>Acum. Pico</th>
                                    <th></th>
                                </tr>
                                <?php $pico=0;$i=0; ?>
                                @foreach($clients as $client)
                                    <?php
                                        $i++;
                                        if($i<=$interval->days){
                                            $pico+=$client->amount-$client->quote;
                                        }

                                        if($pico>=0)
                                            $clase='text-blue';
                                        else {
                                            $clase='text-red';
                                        }
                                        
                                    ?>
                                    <tr>
                                    	<td>{{$client->number_index}}</td>
                                        <td>{{ date('d/m/Y',strtotime($client->fcobro)) }}</td>
                                        <td>{{$client->quote}}</td>
                                        <td>{{round($client->amount/$client->quote,0)}}</td>
                                        <td>
                                            <!--{{ $client->amount>0? 'P':'N' }}-->
                                            {{ $client->condicion}}
                                        </td>
                                        <td>{{$client->amount}}</td>
                                        <td class='{{ $clase }}'>{{ abs($pico) }} </td>
                                        <td></td>
                                    </tr>
                                    <?php
                                        if(($debe_pagar>=$total_pagado)&&($pico>0)){
                                            $pico=0;
                                        }/*elseif(($debe_pagar<$total_pagado)&&($pico<0)){
                                            $pico=0;
                                        }*/
                                    ?>
                                @endforeach

                                </tbody></table>
                        </div><!-- .widget -->
                    </div>
                </div><!-- .row -->
                <!--
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget p-lg">
                            <h4 class="m-b-lg">Ultimos creditos</h4>
                            @foreach($other_credit as $c)
                                @if((app('request')->input('id_credit'))!=$c->id)
                                    <a href="{{url('summary')}}/?id_credit={{$c->id}}" class="btn btn-info">{{$c->id}}</a>
                                @endif
                            @endforeach
                        </div>
                -->
                        <!-- .widget -->
                <!--
                    </div>
                </div>
                -->

                <!-- .row -->
            </section>
        </div>
    </main>
@endsection
<style>
.grid-container {
  display: grid;
  background-color: blue; /*rgba(255, 255, 255, 0.8);*/
  grid-template-columns: auto auto;
}
.grid-item {
  /*display: grid;*/
  border: 0.2px solid black;
  color:yellow;
}
.text-wwhite{
    color:white;
}
.text-blue{
    color:blue;
    /*text-align:rigth;*/
}
.text-red{
    color:red;
    /*text-align:rigth;*/
}
</style>
