@extends('layouts.app')

@section('content')
    <!-- MODAL ========-->
    <!-- Modal -->
    <div id="modal_pay" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <form class="modal-pay" action="{{url('summary')}}" method="POST" enctype="multipart/form-data">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Abono de cuota</h4>
                    </div>
                    <div class="modal-body main-body">
                        <div class="form-group">
                            <label for="name">Nombres:</label>
                            <input type="text" readonly class="form-control" id="name">
                        </div>
                        <div class="form-group">
                            <label for="credit_id">Número de credito:</label>
                            <input type="text" readonly class="form-control" id="credit_id">
                        </div>
                        <div class="form-group">
                            <label for="amount_value">Valor de venta:</label>
                            <input type="text" readonly class="form-control" id="amount_value">
                        </div>
                        <div class="form-group">
                            <label for="done">Pagado:</label>
                            <input type="text" readonly class="form-control" id="done">
                        </div>
                        <div class="form-group">
                            <label for="saldo">Saldo:</label>
                            <input type="text" readonly class="form-control" id="saldo">
                        </div>
                        <div class="form-group">
                            <label for="payment_number">Valor de cuota:</label>
                            <input type="text" readonly class="form-control" id="payment_quote">
                        </div>
                        <div class="form-group">
                            <label for="done_payment">Cuotas pagadas:</label>
                            <input type="text" readonly class="form-control" id="done_payment">
                        </div>
                        <div class="form-group">
                            <label for="amount">Valor de abono:</label>
                            <input type="number" step="any" min="1" max="" required name="amount" class="form-control" id="amount">
                        </div>
                    </div>
                    <div class="modal-body msg-success hidden">
                        <div class="form-group text-center">
                            <small class="text-color">Pago realizado</small>
                            <h2 class="text-success">0</h2>
                            <small class="text-color">Saldo</small>
                            <h2 class="text-primary">0</h2>
                        </div>
                    </div>
                    <div class="modal-footer main-body">
                        <button type="submit"  class="btn btn-success btn-block btn-md">Guardar pago</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- FIN MODAL ========-->
    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="wrap">
            <section class="app-content">
                <div class="row">
                    <div class="col-md-12">

                        <div class="widget p-lg">
                            <h4 class="m-b-lg">Clientes y Creditos</h4>
                            @if(app('request')->input('hide'))
                                <div class="alert alert-warning alert-custom alert-dismissible">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <h4 class="alert-title">Informacion</h4>
                                    <p>Orden cambiado por encima/debajo de un usuario que saltaste el dia de hoy.</p>
                                </div>
                            @endif
<!-- table agente-route-table -->
                            <table id="" class="agente-route-table" border=0>
                                <thead>
                                <tr>
                                    <th class="hidden">Orden</th>
                                    <th class="morado">Cuotas Faltantes</th>
                                    <th class="verde">Cuotas Pagadas</th>
                                    <th class="amarillo">Saldo</th>
                                    <th>Nombre</th>
                                    <th>Valor Cuota</th>
                                    <th>Valor Pagado</th>
                                    <th colspan="3" style="text-align-last: center;">Acciones</th>
                                    <th>Prioridad</th>
                                </tr>
                                </thead>

                                <tbody>
                                <?php $i=0 ?>
                                @foreach($clients as $client)
                                <form method="POST" class="payment-create" action="{{url('summary')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{ $client->id }}">
                                <?php
                                /*******
                                 * el motor de inferencia para determinar el color del día
                                 * para cada cliente
                                *******/
                                    $datetime1 = date_create($client->fdesde);
                                    $datetime2 = date_create(date(now()));

                                    $interval = date_diff($datetime1, $datetime2);
                                    if($interval->days==0)
                                        $interval->days=1;
                                    $debe_pagar=$interval->days*$client->cuota;
                                    if($debe_pagar==0)
                                        $div_pagar=1;

                                    $clase='';
                                    if($client->condicion=='--'&& $client->quote){
                                        $clase='blanco';
                                    }elseif($client->amount=='NP'){
                                        $clase='negro';
                                    }elseif($client->amount>0&&$client->condicion='P'){
                                        $clase='azul';
                                    }elseif($client->fcobro<date('Y-m-d')&&$client->number_index==$client->payment_number ) {
                                        $clase='amarillo';
                                    }
                                    $prestamo=$client->amount_neto+($client->amount_neto*$client->utility);
                                    $pagado=$prestamo-$client->saldo;
                                    $cuotas_pagadas=$pagado/$client->quote;
                                    $cuotas_faltantes=$client->payment_number-$cuotas_pagadas;
                                ?>
                                    <tr id="td_{{$client->id}}">
                                        <td class="hidden pequeno">{{ $client->order_list}}</td>
                                        <td class="morado">{{ $cuotas_faltantes}}</td>
                                        <td class="verde">{{ $cuotas_pagadas }}</td>
                                        <td id="saldo" class="amarillo">{{$client->saldo}}</td>
                                        <td class="<?php echo $clase;?>">{{$client->user->name}} {{$client->user->last_name}}</td>
                                        <td style='width:5%;'>{{$client->quote}}</td>
                                        <td style='width:10%;'>
                                            <input type="number" step="any" min="1" max="{{$client->saldo}}"
                                            value="" name="amount" class="form-control" id="amount" style="width:70px">
                                        </td>
                                        <td>
                                            <!--
                                            <a href="{{url('payment')}}/{{$client->id}}" class="btn btn-success btn-xs hidden"></i> Pagar</a>
                                            /{{$client->id}}
                                            -->
                                            <button type="submit" id='boton' name='boton' ' {{($client->saldo <1) ? 'disabled': ''}} class="btn btn-success btn-block btn-md">Pagar</button>
                                        
                                        <!--

                                            <a href="{{url('summary/store')}}?id_credit={{$client->id}}" class="btn btn-success btn-xs hidden"></i> Pagar</a>
                                        -->
                                        </form>

                                            <!--<i class="fa fa-money">-->
                                        </td>
                                        <td>
                                            <a href="{{url('summary')}}?id_credit={{$client->id}}" class="btn btn-info btn-xs hidden">
                                            Detalle</a>

                                            <!--<button type="button" class="btn btn-success btn-xs btn-pagar" data-toggle="modal" data-id="{{$client->id}}" data-target="#modal_pay">Pagar</button>
                                            <a href="javascript:void(0)" id_user="{{$client->id_user}}" id_credit="{{$client->id}}" class="btn btn-warning btn-xs ajax-btn btn-pagar"><i class="fa fa-archive "></i> Saltar</a>

                                            <i class="fa fa-history"></i>
                                            -->
                                        </td>
                                        <td style="width:5%;">
                                            <a href="{{url('summary')}}?id_credit={{$client->id}}" class="btn btn-info btn-xs hidden">
                                            Renovar</a>
                                        </td>
                                        <td style="width: 2%;">
                                            <a href="{{url('route')}}/{{$client->order_list}}/edit?id_credit={{$client->id}}&direction=up" class="btn btn-default btn-xs arw-up btn-center-arrow"><i class="fa fa-arrow-up"></i></a>

                                            <a href="{{url('route')}}/{{$client->order_list}}/edit?id_credit={{$client->id}}&direction=down" class="btn btn-default btn-xs arw-down btn-center-arrow">
                                            <i class="fa fa-arrow-down"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody></table>
                        </div><!-- .widget -->
                    </div>
                </div><!-- .row -->
            </section>
        </div>
    </main>
@endsection

<style type="text/css">
    .pequeno{
        width: 5%;
    }
    .morado{
        width: 5%;
        background-color: #800080;
        color: white;
    }
    .verde{
        width: 5%;
        background-color: #008000;
        color:white;
    }
    .amarillo{
        background-color: #FFFF00;
    }
    .blanco{
        width: 100%;
        background-color: white;
    }
    .negro{
        width: 100%;
        background-color: black;
        color:white;
    }
    .azul{
        width: 100%;
        background-color: blue;
        color:white;
    }
#situacion{
    width: max-content;
    margin: 0 auto;
    clear: both;
    border-collapse: unset;
    border-spacing: 0;
}
.situa{
    width: 38px;
}
</style>