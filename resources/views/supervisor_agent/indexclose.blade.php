@extends('layouts.app')

@section('content')
    <!-- APP MAIN ==========-->
    <main id="app-main" class="app-main">
        <div class="wrap">
            <section class="app-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="widget p-lg">
                            <h4 class="m-b-lg">Agentes**</h4>
                            <table class="table supervisor-close-table">
                                <tbody>
                                <tr class="visible-lg">
                                    <th>Nombre</th>
                                    <th>Cartera</th>
                                    <th>Ciudad</th>
                                    <th>Fecha</th>
                                    <th>Accion</th>
                                </tr>
                                @foreach($clients as $client)
                                <?php
                                    $fechas=array();
                                    $fechas=llena_fechas($client->fechas);
                                ?>
                                @if(count($fechas)>0)
                                    <form method="GET" action="{{url('supervisor/cerrar')}}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {{ method_field('GET') }}
                                        <tr>
                                            <td><span class="value">
                                                <input type="hidden" name="id" value="{{$client->id}}">
                                                {{$client->nombre}} {{$client->apellido}}</span></td>
                                            <td><span class="value">
                                                {{$client->ruta}}</span></td>
                                            <td><span class="value">{{$client->address}}</span></td>
                                            <td><span class="value" >
                                                <select name="fechas">
                                                @for($i=0;$i<(count($fechas)-1);$i++)
                                                   <option value="{{ $fechas[$i] }}">
                                                       {{ date('d/m/Y', strtotime($fechas[$i])) }}
                                                   </option> 
                                                @endfor
                                                </select>

                                                </span></td>
                                            <td>
                                                <!--<a href="{{url('supervisor/close')}}/{{$client->id}}/" class="btn btn-danger btn-xs">Cerrar</a>-->
                                                <input type="submit" name="" class="btn btn-danger btn-xs">
                                            </td>
                                        </tr>
                                    @endif
                                </form>
                                @endforeach
                                </tbody></table>
                        </div><!-- .widget -->
                    </div>
                </div><!-- .row -->
            </section>
        </div>
    </main>
@endsection
<!--
    @if($client->show)
    @endif
    -->
<?php
    function llena_fechas($fechas){
        if($fechas!=''){
            $array_fechas=explode("!",$fechas);
        }else{
            $array_fechas=array();
        }
        return $array_fechas;
    }
?>