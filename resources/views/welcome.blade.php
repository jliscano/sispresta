<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="/../assets/images/logo.png"/>

            <title>{{ config('app.name', 'Laravel') }}</title>

<meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS 
        ================================================== -->
    
    <!-- Bootstrap 3-->
    
    <link href="{{ asset('landy/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
	
    <!-- link href="landy/css/bootstrap.min.css" rel="stylesheet" media="screen">-->
    <!-- Google Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900,200italic,300italic,400italic,600italic,700italic,900italic|Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <!-- Template Styles -->
    <link href="{{ asset('landy/css/style.css') }}" rel="stylesheet" media="screen">
    
    <!--link href="landy/css/style.css" rel="stylesheet" media="screen"-->
    </head>
    <body class="antialiased">
        
        <!-- NAVBAR
	      ================================================== -->
	  <nav class="navbar navbar-default" role="navigation">
	  	  <div class="container">
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			      <span class="sr-only">Toggle navigation</span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			    </button>
			    
			    <!--Replace text with your app name or logo image-->
			    <a class="navbar-brand" href="#">Fortune</a>
			    
			  </div>
			  <div class="collapse navbar-collapse navbar-ex1-collapse">
			    <ul class="nav navbar-nav">
			      <li><a onclick="$('header').animatescroll({padding:71});">Inicio</a></li>
			      <!--<li><a onclick="$('.detail').animatescroll({padding:71});">Pantallas</a></li>-->
			      <li><a onclick="$('.features').animatescroll({padding:71});">Características</a></li>
			      <li><a onclick="$('.social').animatescroll({padding:71});">	Social	</a></li>
			      
			@if (Route::has('login'))
                    @auth
                        <li><a href="{{ url('/home') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Home</a></li>
                    @else
                        <li><a href="{{ route('login') }}" class="text-sm text-gray-700 dark:text-gray-500 underline">Ingresar</a></li>

                        @if (Route::has('register'))
                           <!-- <li><a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Registro</a></li>
                           -->
                        @endif
                    @endauth
            @endif

			    </ul>
			  </div>
		  </div>
	  </nav>
	  
	  
	   <!-- HEADER
	   ================================================== -->	  
	  <header>
		 <div class="container">
			 <div class="row">
				 <div class="col-md-12">
					  <h1>Fortune</h1>
					  <p class="lead">Sistema Control de Ventas</p>
					  
					  <div class="carousel-iphone">
					  	<div id="carousel-example-generic" class="carousel slide">
					    
					    <!-- Indicators -->
					    <ol class="carousel-indicators">
					      <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
					      <li data-target="#carousel-example-generic" data-slide-to="2"></li>
					    </ol>
					  
					    <!-- Wrapper for slides -->
					    <div class="carousel-inner">
					      <div class="item active">
					        <img src="landy/img/screenshots/app-1.png" alt="App Screen 1">
					      </div>
					      <div class="item">
					        <img src="landy/img/screenshots/app-2.png" alt="App Screen 2">
					      </div>
					      <div class="item">
					        <img src="landy/img/screenshots/app-3.png" alt="App Screen 3">
					      </div>
					      
					    </div>
					  </div>
					</div>
				</div>	  
			</div>    
		</div>
	 </header>
	  
	  
	  <!-- PURCHASE
	      ================================================== -->
	  <section class="purchase">
		  <div class="container">
			  <div class="row">
				  <div class="col-md-offset-2 col-md-8">
					 <h1>Sencillo e intuitivo.</h1>
					 	    <p class="lead">Cada cosa en su lugar y un lugar para cada cosa</p>
					 	    <!--<button type="button" class="app-store"></button>	-->
				  </div>
			  </div>
		  </div>
	  </section>
	  
	  
	  <!-- PAYOFF 
	      ================================================== -->
		  <!--
	  <section class="payoff">
		<div class="container">
			  <div class="row">
				  <div class="col-md-12">
					  <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</h1>
				  </div>
			  </div>
		  </div>	  
	  </section>
	-->
	  
	  <!-- DETAILS 
	      ================================================== -->
		  <!--
	  <section class="detail">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div id="carousel-example-generic-2" class="carousel slide">
					-->							
					  <!-- Wrapper for slides -->
					  <!--
					  <div class="carousel-inner">
					    <div class="item active">
					      	<div class="row">
					      		<div class="col-sm-12 col-md-offset-1 col-md-6">
					      			<h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h1>
					      			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					      		</div>
					      		<div class="col-sm-12 col-md-5">
					      			<div class="app-screenshot">
					      				<img src="landy/img/screenshots/app-1.png" class="img-responsive" alt="App Screen 1">
					      			</div>
					      		</div>
					      	</div>
					    </div>
					    <div class="item">
					    	<div class="row">
					    		<div class="col-sm-12 col-md-offset-1 col-md-6">
					    			<h1>Sed do eiusmod tempor incididunt ut labore et dolore magna.</h1>
					    			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					    		</div>
					    		<div class="col-sm-12 col-md-5">
					    			<div class="app-screenshot">
					    				<img src="landy/img/screenshots/app-2.png" class="img-responsive" alt="App Screen 2">
					    			</div>
					    		</div>
					    	</div>
						</div>
					    <div class="item">
					      <div class="row">
					      	<div class="col-sm-12 col-md-offset-1 col-md-6">
					      		<h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h1>
					      		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna.</p>
					      	</div>
					      	<div class="col-sm-12 col-md-5">
					      		<div class="app-screenshot">
					      			<img src="landy/img/screenshots/app-3.png" class="img-responsive" alt="App Screen 3">
					      		</div>
					      	</div>
					      </div>
					    </div>
					  </div>
					-->
					  <!-- Indicators -->
					  <!--
					  <ol class="carousel-indicators">
					    <li data-target="#carousel-example-generic-2" data-slide-to="0" class="active"></li>
					    <li data-target="#carousel-example-generic-2" data-slide-to="1"></li>
					    <li data-target="#carousel-example-generic-2" data-slide-to="2"></li>
					  </ol>		
					</div>
				</div>
			</div>
		</div>
	</section>
-->
	  
	  <!-- FEATURES
	      ================================================== -->
	  <section class="features">
		  <div class="container">
			  <div class="row">
				
				  <div class="col-md-4">
					  <div class="circle"><i class="icon-bookmark"></i></div>
					  <h2>Fácil Uso</h2>
					  <p>Diseñado para los usuarios más difíciles.</p>
				  </div>
				
				  <div class="col-md-4">
					  <div class="circle"><i class="icon-keypad"></i></div>
					  <h2>Adaptado al Perú</h2>
					  <p>Para satisfacer las necesidades de personas exigentes, quienes necesitan respuestas en forma rápida y precisa.</p>
				  </div>
				 
				  <div class="col-md-4">
					  <div class="circle"><i class="icon-like"></i></div>
					  <h2>Diseño Responsivo</h2>
					  <p>Las pantallas se adaptan al tamaño del dispositivo del usuario ya sea movil o PC.</p>
				  </div>
				  
			  </div>
		  </div>
	  </section>
	  
	
	 <!-- SOCIAL
	     ================================================== -->
	  <section class="social">
	  	<div class="container">
	  		  <div class="row">
	  			  <div class="col-md-12">
	  			  	<h2>Conecta con nosotros</h2>
	  			   	<a class="icon-facebook" target="_BLANK" href="https://www.facebook.com/jliscanov/"></a>
	  			   	<a class="icon-twitter" target="_BLANK" href="https://twitter.com/Compadrament"></a>
	  			   	<!--<a class="icon-google"></a>-->
	  			   	<a class="icon-instagram" href="https://www.instagram.com/jliscanova/" target="_BLANK" ></a>
	  			   	<!--<a class="icon-pinterest"></a>-->
	  			   </div>
	  		  </div>
	  	  </div>	  
	  </section>
	  
	
	 <!-- GET IT 
	     ================================================== -->
	  <section class="get-it">
	  	<div class="container">
	  		<div class="row">
				<!--
	  			<div class="col-md-12">
	  				<h1>Avaialable now on the App Store</h1>
	  				<p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
	  				<button type="button" class="app-store"></button>
	  			</div>
			-->
	  			<div class="col-md-12">
	  				<hr />
		  			<ul>
	                	<li><a href="#link-here">Contact</a></li>
	                	<li><a href="#link-here">Twitter</a></li>
	                	<li><a href="#link-here">Press</a></li>
	                	<li><a href="#link-here">Support</a></li>
	                	<li><a href="#link-here">Developers</a></li>
	                	<li><a href="#link-here">Privacy</a></li>
                	</ul>
	  			</div>
	  		</div>
	  	</div>
	  </section>
	  

	 <!-- JAVASCRIPT
	     ================================================== -->
	<script src="{{ asset('landy/js/jquery.js') }}" defer></script>
	<script src="{{ asset('landy/js/bootstrap.min.js') }}" defer></script>
	<script src="{{ asset('landy/js/animatescroll.js') }}" defer></script>
	<script src="{{ asset('landy/js/scripts.js') }}" defer></script>
	<script src="{{ asset('landy/js/retina.min.js') }}" defer></script>
	<!--
    <script src="landy/js/jquery.js"></script>
    <script src="landy/js/bootstrap.min.js"></script>
    <script src="landy/js/animatescroll.js"></script>
    <script src="landy/js/scripts.js"></script>
    <script src="landy/js/retina.min.js"></script>
-->
   <!-- </body> -->
</html>
